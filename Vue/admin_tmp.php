<!doctype html>
<html lang="fr">
    <head>
        <meta charset="UTF-8" />
        <title><?= $titre ?></title>
    </head>
    <body>
            <header>
                <h1 id="titreBlog">Bienvenue <?php echo $_SESSION['user']['prenom']; ?></h1>
            </header>
            <div id="contenu">
                <?= $contenu ?>
            </div> <!-- #contenu -->

            <div class="nav">
              <a href="index.php?page=admin">Tableau de bord</a>
              <br>
              <a href="index.php?page=article-new">Créer un article</a>
              <br>
              <a href="index.php?page=articles-user">Gérer mes articles</a>
              <br>
              <a href="index.php?page=admin-users">Gérer les utilisateurs</a>
              <br>
              <a href="index.php?page=admin-articles">Gérer les articles en attente</a>
              <br>
              <a href="index.php?page=user-profil">Gérer mon profil</a>
              <br>
              <a href="index.php?action=deco">Deconnexion</a>
            </div>
            <footer id="piedBlog">
            </footer>
        </div> <!-- #global -->
    </body>
</html>
