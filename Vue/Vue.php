<?php

class Vue {

    // Nom du fichier associé à la vue
    private $fichier;
    // Titre de la vue (défini dans le fichier vue)
    private $titre;

    private $mode;

    public function __construct($action, $mode = '') {
        // Détermination du nom du fichier vue à partir de l'action
        $this->fichier = "Vue/vue" . $action . ".php";
        $this->mode = $mode;
    }

    // Génère et affiche la vue
    public function generer($donnees = array()) {
      // Génération de la partie spécifique de la vue
      $contenu = $this->genererFichier($this->fichier, $donnees);
        if($this->mode == 'user'){
          // Génération du gabarit commun utilisant la partie spécifique
          $vue = $this->genererFichier('Vue/user_tmp.php',
                  array('titre' => $this->titre, 'contenu' => $contenu));
        }else if($this->mode == 'admin'){
            // Génération du gabarit commun utilisant la partie spécifique
            $vue = $this->genererFichier('Vue/admin_tmp.php',
                    array('titre' => $this->titre, 'contenu' => $contenu));
          }else{
          // Génération du gabarit commun utilisant la partie spécifique
          $vue = $this->genererFichier('Vue/gabarit.php',
                  array('titre' => $this->titre, 'contenu' => $contenu));
        }
        // Renvoi de la vue au navigateur
        echo $vue;
    }

    // Génère un fichier vue et renvoie le résultat produit
    private function genererFichier($fichier, $donnees) {
        if (file_exists($fichier)) {
            // Rend les éléments du tableau $donnees accessibles dans la vue
            extract($donnees);
            // Démarrage de la temporisation de sortie
            ob_start();
            // Inclut le fichier vue
            // Son résultat est placé dans le tampon de sortie
            require $fichier;
            // Arrêt de la temporisation et renvoi du tampon de sortie
            return ob_get_clean();
        }
        else {
            throw new Exception("Fichier '$fichier' introuvable");
        }
    }

}
