<?php $this->titre = "User"; ?>
  <h2>Modifier mon profil</h2>
  <?php if(isset($erreurs)){
      echo $erreurs;
  }
  $user = $_SESSION['user'];
  ?>
  <form method="post" action="index.php?page=user-profil&action=update">
      <input id="nom" name="nom" type="text" placeholder="Nom" value="<?php echo $user['nom']; ?>" />
      <br />
      <input id="prenom" name="prenom" type="text" placeholder="Prenom" value="<?php echo $user['prenom']; ?>" />
      <br />
      <input id="mail" name="mail" type="text" placeholder="mail" value="<?php echo $user['mail']; ?>" />
      <br />
      <input id="ddn" name="ddn" type="text" placeholder="date de naissance JJ/MM/AAAA" value="<?php echo $user['ddn']; ?>" />
      <br />
      <input type="submit" value="Mettre à jour" />
  </form>
  <h3>Modifier le mot de passe</h3>
  <form method="post" action="index.php?page=user-profil&action=update-mdp">
      <input id="mdp1" name="mdp1" type="password" placeholder="Mot de passe" />
      <br />
      <input id="mdp2" name="mdp2" type="password" placeholder="Confirmer mot de passe" />
      <br />
      <input type="submit" value="Mettre à jour" />
  </form>
