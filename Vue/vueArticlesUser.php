<?php $this->titre = "tdb";
    $user = $_SESSION['user'];
?>
<h2>Mes articles</h2>
<h3>Articles en attente</h3>
<?php foreach ($articlesWait as $article): ?>
    <article class="">
      <h4><?php echo $article['title']; ?></h4>
    </article>
<?php endforeach; ?>
<h3>Articles refusés</h3>
<?php foreach ($articlesRefused as $article): ?>
    <article class="">
      <h4><?php echo $article['title']; ?></h4>
    </article>
<?php endforeach; ?>
<h3>Articles publiés</h3>
<?php foreach ($articlesPublish as $article): ?>
  <a href="index.php?page=article&articleId=<?php echo $article['id']; ?>">
    <article class="">
      <h4><?php echo $article['title']; ?></h4>
    </article>
  </a>
<?php endforeach; ?>
