<!doctype html>
<html lang="fr">
    <head>
        <meta charset="UTF-8" />
        <title><?= $titre ?></title>
    </head>
    <body>
            <header>
                <a href="index.php"><h1 id="titreBlog">Wiki</h1></a>
            </header>
            <div id="contenu">
                <?= $contenu ?>
            </div> <!-- #contenu -->
            <footer id="piedBlog">
            </footer>
        </div> <!-- #global -->
    </body>
</html>
