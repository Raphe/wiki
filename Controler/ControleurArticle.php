<?php

require_once 'Model/Article.php';
require_once 'Model/Util.php';
require_once 'Vue/Vue.php';

class ControleurArticle {
    private $article;
    private $util;
    public function __construct() {
        $this->article = new Article();
        $this->util = new Util();
    }

    public function detailArticle($idArticle) {
        $article = $this->article->getArticle($idArticle);
        $vue = new Vue("ArticleDetail", 'user');
        $vue->generer(array('article' => $article));
    }

    public function createArticle() {
        $title = $this->util->getParametre($_POST, 'title');
        $description = $this->util->getParametre($_POST, 'description');
        session_start();
        $userId = $_SESSION['user']['id'];
        $idArticle = $this->article->addArticle($title, $description, $userId);
        $url = $this->util->uploadFichier($idArticle, 'imgArticle');
        $this->article->majArticleImg($url, $idArticle);

        header('Location: index.php?page=articles-user');
        exit;
    }


}
