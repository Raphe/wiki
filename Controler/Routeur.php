<?php

require_once 'Controler/ControleurAccueil.php';
require_once 'Controler/ControleurUser.php';
require_once 'Controler/ControleurArticle.php';
require_once 'Vue/Vue.php';
class Routeur {

    private $ctrlAccueil;
    private $ctrlUser;
    private $ctrlArticle;
    private $publicPages;

    public function __construct() {
        $this->ctrlAccueil = new ControleurAccueil();
        $this->ctrlUser = new ControleurUser();
        $this->ctrlArticle = new ControleurArticle();
        $this->publicPages = array('connexion', 'inscription');
        $this->AdminPages = array();
    }

    // Route une requête entrante : exécution l'action associée
    public function routerRequete() {
        try {
            if (isset($_GET['page'])) {
                if(!in_array($_GET['page'], $this->publicPages)){
                  session_start();
                  $this->sessionVerify();
                }

                if ($_GET['page'] == 'connexion') {
                  if(isset($_GET['action']) && $_GET['action'] == 'co'){
                    try{
                      $this->ctrlUser->coUser();
                    }catch (Exception $e) {
                          $erreurs = $e->getMessage();
                          $vue = new Vue("Connexion");
                          $vue->generer(array('erreurs' => $erreurs));
                      }
                  }else{
                    $vue = new Vue("Connexion");
                    $vue->generer();
                  }
                }else if($_GET['page'] == 'inscription'){
                  if(isset($_GET['action']) && $_GET['action'] == 'create'){
                    try {
                      $this->ctrlUser->createUser();
                    }  catch (Exception $e) {
                          $erreurs = $e->getMessage();
                          $vue = new Vue("Inscription");
                          $vue->generer(array('erreurs' => $erreurs));
                      }
                  }else{
                    $vue = new Vue("Inscription");
                    $vue->generer();
                  }
                }else if($_GET['page'] == 'user'){
                  $this->ctrlUser->indexUser();
                }else if($_GET['page'] == 'user-profil'){
                  if(isset($_GET['action'])){
                    try {
                      if( $_GET['action'] == 'update'){
                        $this->ctrlUser->updateUser($_SESSION['user']['id']);
                      }else if( $_GET['action'] == 'update-mdp'){
                        $this->ctrlUser->updateMdp($_SESSION['user']['id']);
                      }

                    }  catch (Exception $e) {
                          $erreurs = $e->getMessage();
                          $this->ctrlUser->detailUser($_SESSION['user']['id'], $erreurs);
                      }
                  }else{
                    $this->ctrlUser->detailUser($_SESSION['user']['id']);
                  }
                }else if($_GET['page'] == 'article-new'){
                  if(isset($_GET['action']) && $_GET['action'] == 'create'){
                    try {
                      $this->ctrlArticle->createArticle();
                    }  catch (Exception $e) {
                          $erreurs = $e->getMessage();
                          $vue = new Vue("ArticleNew", 'user');
                          $vue->generer(array('erreurs' => $erreurs));
                      }
                  }else{
                    $vue = new Vue("ArticleNew", 'user');
                    $vue->generer();
                  }
                }else if($_GET['page'] == 'articles-user'){
                    $this->ctrlUser->articlesUser($_SESSION['user']['id']);
                }else if ($_GET['page'] == 'article' && isset($_GET['articleId'])) {
                  $this->ctrlArticle->detailArticle($_GET['articleId']);
                }
                else
                    throw new Exception("page non valide");
            }
            else {  // aucune action définie : affichage de l'accueil
                if(isset($_GET['action']) && $_GET['action'] == 'deco'){
                  session_start();
                  session_destroy();
                }
                $this->ctrlAccueil->accueil();
            }
        }
        catch (Exception $e) {
            $this->erreur($e->getMessage());
        }
    }

    // Affiche une erreur
    private function erreur($msgErreur) {
        $vue = new Vue("Erreur");
        $vue->generer(array('msgErreur' => $msgErreur));
    }

    private function sessionVerify(){
      if(!isset($_SESSION['user'])){
        session_destroy();
        header('Location: index.php');
        exit;
      }
    }


}
