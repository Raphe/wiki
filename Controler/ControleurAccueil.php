<?php

require_once 'Model/Article.php';
require_once 'Vue/Vue.php';

class ControleurAccueil {
  private $articles;

  public function __construct() {
      $this->articles = new Article();
  }
// Affiche la liste de tous les billets du blog
    public function accueil() {
        $vue = new Vue("Accueil");
        $articlesList = $this->articles->getArticles('publish');
        $vue->generer(array('articles' => $articlesList));
    }

}
