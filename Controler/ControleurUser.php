<?php

require_once 'Model/User.php';
require_once 'Model/Article.php';
require_once 'Model/Util.php';
require_once 'Vue/Vue.php';

class ControleurUser {
    private $articles;
    private $user;
    private $util;
    private $affichage;

    public function __construct() {
        $this->user = new User();
        $this->util = new Util();
        $this->articles = new Article();
        $this->affichage = 'user';
    }

    public function indexUser() {
        $articlesList = $this->articles->getArticles('publish');
        $vue = new Vue("UserIndex", $this->affichage);
        $vue->generer(array('articles' => $articlesList));
    }

    public function detailUser($idUser, $erreurs = false) {
        $user = $this->user->getUser($idUser);
        $vue = new Vue("UserDetail", $this->affichage);
        if($erreurs){
          $vue->generer(array('user' => $user, 'erreurs' => $erreurs));
        }else{
            $vue->generer(array('user' => $user));
        }
    }

    public function articlesUser($idUser) {
        $articlesPublish = $this->articles->getArticles('publish', $idUser);
        $articlesWait = $this->articles->getArticles('waiting', $idUser);
        $articlesRefused = $this->articles->getArticles('refused', $idUser);
        $vue = new Vue("ArticlesUser", $this->affichage);
        $vue->generer(array('articlesPublish' => $articlesPublish, 'articlesWait' => $articlesWait, 'articlesRefused' => $articlesRefused));
    }

    public function updateUser($idUser) {
        $mail = $this->util->getParametre($_POST, 'mail', 'mail');
        $nom = $this->util->getParametre($_POST, 'nom');
        $prenom = $this->util->getParametre($_POST, 'prenom');
        $ddn = $this->util->getParametre($_POST, 'ddn', 'date');
        $ddn = $this->util->dateSql($ddn);

        $idUser = $this->user->majUser($nom, $prenom, $mail, $ddn, $idUser);
        session_start();
        $_SESSION['user'] = $this->user->getUser($idUser);
        $ddn = $this->util->dateFormat($_SESSION['user']['ddn']);
        $_SESSION['user']['ddn'] = $ddn;

        header('Location: index.php?page=user-profil');
        exit;

    }

    public function updateMdp($idUser) {
        $mdp = $this->util->hashMdp($_POST, 'mdp1', 'mdp2');

        $idUser = $this->user->majMdp($mdp, $idUser);
        session_start();
        $_SESSION['user'] = $this->user->getUser($idUser);
        $ddn = $this->util->dateFormat($_SESSION['user']['ddn']);
        $_SESSION['user']['ddn'] = $ddn;

        header('Location: index.php?page=user-profil');
        exit;

    }

    public function createUser() {
        $mail = $this->util->getParametre($_POST, 'mail', 'mail');
        $nom = $this->util->getParametre($_POST, 'nom');
        $prenom = $this->util->getParametre($_POST, 'prenom');
        $mdp = $this->util->hashMdp($_POST, 'mdp1', 'mdp2');
        $ddn = $this->util->getParametre($_POST, 'ddn', 'date');
        $ddn = $this->util->dateSql($ddn);
        if(!$this->user->userExists($mail)){
          $idUser = $this->user->addUser($nom, $prenom, $mail, $ddn, $mdp);
          session_start();
          $_SESSION['user'] = $this->user->getUser($idUser);
          $ddn = $this->util->dateFormat($_SESSION['user']['ddn']);
          $_SESSION['user']['ddn'] = $ddn;
          header('Location: index.php?page=user');
          exit;
        }else{
          throw new \Exception("Cet e-mail est déja utilisé");

        }
    }

    public function coUser() {
        $mail = $this->util->getParametre($_POST, 'mail', 'mail');
        $mdp = $this->util->getParametre($_POST, 'mdp');
        if(!$idUser = $this->user->userExists($mail)){
          throw new \Exception("Problème dans les données de connexion");
        }else{
          $user = $this->user->getUser($idUser);
          if(password_verify($mdp, $user['mdp'])){
            session_start();
            $_SESSION['user'] = $user;
            $ddn = $this->util->dateFormat($_SESSION['user']['ddn']);
            $_SESSION['user']['ddn'] = $ddn;
            header('Location: index.php?page=user');
            exit;
          }else{
            throw new \Exception("Problème dans les données de connexion");
          }
        }
    }

}
