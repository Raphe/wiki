<?php

require_once 'Model/DB.php';

class User {
    protected $bdd;

    public function __construct() {
        $this->bdd = new DB();
    }

    public function getUser($idUser) {
        $sql = 'select id_user as id,'
                . ' nom_user as nom,'
                . ' prenom_user as prenom,'
                . ' mail_user as mail,'
                . ' ddn_user as ddn,'
                . ' mdp_user as mdp,'
                . ' droits_user as droits'
                . ' FROM user'
                . ' WHERE id_user=?';
        $user = $this->bdd->executerRequete($sql, array($idUser));
        if ($user->rowCount() > 0)
            return $user->fetch();  // Accès à la première ligne de résultat
        else
            throw new Exception("Aucun utilisateur ne correspond à l'identifiant '$idUser'");
    }

    // Ajoute un contact dans la base
    public function addUser($nom, $prenom, $mail, $ddn, $mdp) {
        $sql = 'INSERT INTO user(nom_user, prenom_user, mail_user, ddn_user, mdp_user)'
        . ' values(?, ?, ?, ?, ?)';
        $this->bdd->executerRequete($sql, array($nom, $prenom, $mail, $ddn, $mdp));
        $idUser = $this->bdd->lastInsertId();
        return $idUser;
    }

    public function majUser($nom, $prenom, $mail, $ddn, $id) {
        $sql = 'UPDATE user SET nom_user = :nom, prenom_user = :prenom, mail_user = :mail, ddn_user = :ddn'
            . ' WHERE id_user = :id';
        $this->bdd->executerRequete($sql, array(
                                    'nom' => $nom,
                                    'prenom' => $prenom,
                                    'mail' => $mail,
                                    'ddn' => $ddn,
                                    'id' => intval($id))
                                );
        return $id;
    }

    public function majMdp($mdp, $id) {
        $sql = 'UPDATE user SET mdp_user = :mdp'
            . ' WHERE id_user = :id';
        $this->bdd->executerRequete($sql, array(
                                    'mdp' => $mdp,
                                    'id' => intval($id))
                                );
        return $id;
    }

    public function userExists($mail) {
        $sql = 'select id_user as id'
                . ' FROM user'
                . ' WHERE mail_user= ? ';
        $user = $this->bdd->executerRequete($sql, array($mail));
        if ($user->rowCount() > 0){
            $user = $user->fetch();
            return $user['id'];
        } // Accès à la première ligne de résultat
        else
            return false;
    }

    public function coUser($mail, $mdp) {
        if($userId = $this->votant->userExists($mail)){
          $user = getUser($userId);
          if(password_verify($mdp, $user['mdp'])){
            header('Location: index.php?page=user&id='.$userId);
            exit;
          }else{
            throw new Exception("Mauvais identifiant ou mot de passe");
          }
        }else{
          throw new Exception("Mauvais identifiant ou mot de passe");
        }
        // Actualisation de l'affichage du billet

    }



}
