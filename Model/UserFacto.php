<?php

require_once 'Model/DB.php';
require_once 'Model/User.php';
require_once 'Model/Admin.php';

abstract class UserFacto
{
  private $bdd;

  public function __construct() {
      $this->bdd = new DB();
  }

    public static function create($idUser)
    {
      $droits = getDroits($idUser);
      if($droit==1){
          return new User($idUser);
      }else if($type == 9){
          return new Admin($idUser);
      }else{
        throw new Exception("Le type de droits pour '$idUser' n'est pas reconnu");
      }
    }

    public function getDroits($idUser) {
        $sql = 'select droits_user as droits'
                . ' FROM user'
                . ' WHERE id_user=?';
        $user = $this->bdd->executerRequete($sql, array($idUser));
        if ($user->rowCount() > 0)
            $user = $user->fetch();
            return $user['droits'];  // Accès à la première ligne de résultat
        else
            throw new Exception("Aucun utilisateur ne correspond à l'identifiant '$idUser'");
    }
}
