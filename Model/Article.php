<?php

require_once 'Model/DB.php';

class Article {
    private $bdd;

    public function __construct() {
        $this->bdd = new DB();
    }

    public function getArticles($mode = 'all', $user = false) {
        $sql = 'SELECT id_article AS id,'
                . ' title_article AS title,'
                . ' desc_article AS description,'
                . ' img_article AS img,'
                . ' date_article AS date,'
                . ' statut_article AS statut,'
                . ' auteur_article AS auteur,'
                . ' nom_user AS userNom,'
                . ' prenom_user AS userPrenom'
                . ' FROM article'
                . ' LEFT JOIN user ON article.auteur_article = user.id_user';
        if($mode == 'refused'){
          $sql .= ' WHERE statut_article = 0';
        }else if ($mode == 'waiting'){
          $sql .= ' WHERE statut_article = 1';
        }else if ($mode == 'publish'){
          $sql .= ' WHERE statut_article = 2';
        }
        if($user){
          if ($mode == 'all') {
            $sql .= ' WHERE auteur_article = :id';
          }else{
            $sql .= ' AND auteur_article = :id';
          }

          $articles = $this->bdd->executerRequete($sql, array('id' => $user));
        }else{
          $articles = $this->bdd->executerRequete($sql, array());
        }
        return $articles->fetchAll();
    }

    public function getArticle($idArticle) {
        $sql = 'SELECT id_article AS id,'
                . ' title_article AS title,'
                . ' desc_article AS description,'
                . ' img_article AS img,'
                . ' date_article AS date,'
                . ' statut_article AS statut,'
                . ' auteur_article AS auteur,'
                . ' nom_user AS userNom,'
                . ' prenom_user AS userPrenom'
                . ' FROM article'
                . ' LEFT JOIN user ON article.auteur_article = user.id_user'
                . ' WHERE id_article = ?';
        $article = $this->bdd->executerRequete($sql, array($idArticle));
        if ($article->rowCount() > 0)
            return $article->fetch();  // Accès à la première ligne de résultat
        else
            throw new Exception("Aucun article ne correspond à l'identifiant '$idArticle'");
    }

    // Ajoute un contact dans la base
    public function addArticle($title, $description, $userId) {
        $sql = 'INSERT INTO article(title_article, desc_article, auteur_article, date_article)'
        . ' values(?, ?, ?, NOW())';
        $this->bdd->executerRequete($sql, array($title, $description, $userId));
        $idArticle = $this->bdd->lastInsertId();
        return $idArticle;
    }

    // Ajoute un contact dans la base
    public function majArticleImg($url, $idArticle) {

      $sql = 'UPDATE article SET img_article = :img'
          . ' WHERE id_article = :id';
      $this->bdd->executerRequete($sql, array(
                                  'img' => $url,
                                  'id' => intval($idArticle))
                              );
      return $idArticle;
    }

}
