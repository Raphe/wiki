<?php

require_once 'Model/DB.php';
require_once 'Model/User.php';
require_once 'Model/Article.php';

class Admin extends User {
  private $article;

  public function __construct() {
      $this->article = new Article();
  }

  public function changeStatus($articleId, $mode){
    if($mode == 'accept'){
      $statut = 2;
    }else if($mode == 'refused'){
      $statut = 0;
    }
    $sql = 'UPDATE article SET statut = :statut'
        . ' WHERE id_article = :id';
    $this->bdd->executerRequete($sql, array(
                                'statut' => $statut,
                                'id' => intval($idArticle))
                            );
    return $idArticle;
  }
  }
}
