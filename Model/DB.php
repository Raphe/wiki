<?php

class DB
{

    private static $bdd; // L'attribut qui stockera l'instance unique


    public function __construct() {
      if (is_null(self::$bdd)) {
          self::$bdd = new PDO('mysql:host=localhost;dbname=wiki;charset=utf8',
                  'root', '',
                  array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
      }
      return self::$bdd;
    }
    /**
    * La méthode statique qui permet d'instancier ou de récupérer l'instance unique
    **/

    /**
     * Exécute une requête SQL éventuellement paramétrée
     *
     * @param string $sql La requête SQL
     * @param array $valeurs Les valeurs associées à la requête
     * @return PDOStatement Le résultat renvoyé par la requête
     */
      public function executerRequete($sql, $params = null) {
          if ($params == null) {
              $resultat = self::$bdd->query($sql); // exécution directe
          }
          else {
              $resultat = self::$bdd->prepare($sql);  // requête préparée
              $resultat->execute($params);
          }
          return $resultat;
      }


      public function lastInsertId() {
          return self::$bdd->lastInsertId();
      }

}
