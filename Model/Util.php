<?php

require_once 'Model/DB.php';
require_once('Vendor/claviska/SimpleImage.php');
class Util {
    private $bdd;

    public function __construct() {
        $this->bdd = new DB();
    }

    public function getParametre($tableau, $nom, $verify = false) {
        if (isset($tableau[$nom]) && !empty($tableau[$nom])) {
          if(!$verify){
              return $tableau[$nom];
          }else if($verify == 'mail'){
            if(filter_var($tableau[$nom], FILTER_VALIDATE_EMAIL)){
              return $tableau[$nom];
            }else{
              throw new Exception("L'e-mail n'est pas valide");
            }
          }else if($verify = 'date'){
            if($this->validateDate($tableau[$nom])){
              return $tableau[$nom];
            }else{
              throw new Exception("La date n'est pas valide");
            }
          }
        }
        else
            throw new Exception("Paramètre '$nom' absent");
    }

    public function hashMdp($tableau, $mdp1, $mdp2) {
      if (isset($tableau[$mdp1]) && isset($tableau[$mdp2])) {
        if($tableau[$mdp1] == $tableau[$mdp2]){
          return password_hash($tableau[$mdp1], PASSWORD_DEFAULT);
        }else{
          throw new Exception("Les mots de passe ne sont pas identiques");
        }
      }
      else
          throw new Exception("Veuillez remplir les 2 champs mdp");
    }

    private function validateDate($date, $format = 'd/m/Y') {
        $d = DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) == $date;
    }

    public function dateSql($date){
      $date = DateTime::createFromFormat('d/m/Y', $date);
      return $date->format('Y-m-d');
    }

    public function dateformat($date){
      $date = DateTime::createFromFormat('Y-m-d', $date);
      return $date->format('d/m/Y');
    }

    function uploadFichier($idArticle, $nomInput, $dossier = "Uploads", $tailleMax = 4){

  		$tailleMaxAffiche = $tailleMax.' Mo';

  			//liste mime image
  			$listMime = array(
  				".jpeg" => "image/jpeg",
  				".png" => "image/png",
  				".gif" => "image/gif"
  			);
  		//On créé un tableau avec les type MIME qu'on accepte

  		$listeExtensions = array_flip($listMime); // Inverse les clés et les valeurs d'un tableau

  		//traitement du fichier
  		// if le serveut à été chargé avec la méthode post (environ égal if(!empty($_POST)))
  			if(empty($_FILES)){
  				//Le tableau $_FILES est complêtement vide (le sous tableau du fichier envoyé n'existe même pas)
  				//On a essayé d'envoyer un fichier dépassant la taille limite du parametre post_max_size défini dans le php.ini
  				return false;
  			}else{
  				//on continu les tests en regardant les messages d'erreurs renvoyés par PHP
  				$fileError = $_FILES[$nomInput]["error"];
  				switch ($fileError) {
  					case '1':
              throw new Exception("Error 1");
  						break;

  					case '2':
  						throw new Exception("Error 2");
  						break;

  					case '3':
  						throw new Exception("Error 3");
  						break;

  					case '4':
  						throw new Exception("Error 4");
  						break;

  					case '6':
  						throw new Exception("Error 6");
  						break;

  					case '7':
  						throw new Exception("Error 7");
  						break;

  					case '8':
  						throw new Exception("Error 8");
  						break;

  					default:
  						$tmpFile = $_FILES[$nomInput]["tmp_name"];
  						$finfo = finfo_open(FILEINFO_MIME_TYPE);
  						$typeMime = finfo_file($finfo, $tmpFile);
  						finfo_close($finfo);


  						if (!in_array($typeMime, $listMime)) {
                throw new Exception("Le format de votre fichier n'est pas accepté.");

  						}else{
  							$extension = array_search($typeMime, $listMime);

  							if (!is_dir($dossier)) {
  								mkdir($dossier);
  							}

  								$image = new \claviska\SimpleImage();
  								try{
  									$image->fromFile($tmpFile);
  									if(isset($image->getExif()["Orientation"])){
  										$image->autoOrient();
  									}


  									//Redimentionnement d'une image "thumbnail" et engistrement
  									$image->thumbnail(150, 150)->toFile($dossier."/".$idArticle."-thumb".$extension, $typeMime, 70);

  									return $dossier."/".$idArticle."-thumb".$extension;

  								}catch(Exception $err) {
  								  // Handle errors
  								  $retour =  $err->getMessage();
  								}


  						}

  				}
  			}

  		return $retour;
  	}

}
